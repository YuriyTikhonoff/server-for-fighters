const validationError  = require('../errors/validationError');
const notFoundError = require('../errors/notFoundError');

const responseMiddleware = (req, res, next) => {
    if (res.data){
            res.json(res.data);
    } else if(res.data === null){
                res.status(404).json({
                    error: true,
                    message: "Not found"
                })
    } else {
        res.status(400).json({
            error: true,
            message: res.err.message
        })
    }

}

exports.responseMiddleware = responseMiddleware;