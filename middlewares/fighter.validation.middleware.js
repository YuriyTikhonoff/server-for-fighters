const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const { name, health, power, defense } = req.body
    fighter.name = name;
    fighter.health = health;
    fighter.power = power;
    fighter.defense = defense;

    if (fighter.name && fighter.health && fighter.power && fighter.defense) {
        const powerIsNumber = typeof fighter.power === 'number';
        const defenceIsNumber = typeof fighter.defense === 'number';
        if(!powerIsNumber && !defenceIsNumber){
            res.body = res.status(400).json({
                error: true,
                message: "Power or defence is not a number"
            })
            next(res.body);
        }
        const powerMoreThan = fighter.power < 100;
        const powerLessThan = fighter.power > 0;
        const defenceMoreThan = fighter.defense < 10;
        const defenceLessThan = fighter.defense > 1;
        const powerValid = powerLessThan && powerMoreThan;
        const defenceValid = defenceMoreThan && defenceLessThan;
        if (powerValid && defenceValid) {
            req.body = {     
                name : fighter.name,
                health : fighter.health,
                power : fighter.power,
                defense : fighter.defense,
                }
            next();
        } else {
            res.body = res.status(400).json({
                error: true,
                message: "Power or defence is too high"
            })
            next(res.body);
        }
    }else{
        res.body = res.status(400).json({
            error: true,
            message: "Fighter entity to create is not valid"
        })
        next(res.body);
    }
}

const updateFighterValid = (req, res, next) => {
    const { name, health, power, defense } = req.body
    const updateFighterInfo = req.body;

    const validatePropNames = (fighterInfo) => {
        const fieldsToBeUpdated = ["name", "health", "power", "defense"];
        const invalidProps = []

        for (let [key, value] of Object.entries(fighterInfo)) {
            if (!fieldsToBeUpdated.includes(key)) {
                invalidProps.push(key)
            }
        }

        if(invalidProps.length !== 0){
            res.body = res.status(400).json({
                error: true,
                message: `Request body contains unknown property: ${invalidProps}`
            });
        }
    }

    validatePropNames(updateFighterInfo);

    if(power){
        if((power >= 100) || (power < 0) || (typeof power !== 'number')){
            res.body = res.status(400).json({
                error: true,
                message: `Power is not valid`
            })
        }
    }

    if(defense){
        if((defense >= 10) || (defense < 0) || (typeof defense !== 'number')){
            res.body = res.status(400).json({
                error: true,
                message: `Defense is not valid`
            })
        }
    }

    next(res.body);
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;